package br.com.giovaneudemy.javabasico.casting;

public class Casting {
    public static void main(String[] args) {
        int a, b;
        double resultado;

        a = 5;
        b = 2;
        resultado = (double) a/b;

        System.out.println(resultado);


        // 5/2 = dividindo os dois inteiros o Java vai executar 2.0, nao 2.5, pois resultado é do tipo double
        // example casting = (double) conversão explicita dos valores
    }
}
