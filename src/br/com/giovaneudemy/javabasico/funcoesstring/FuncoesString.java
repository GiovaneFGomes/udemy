package br.com.giovaneudemy.javabasico.funcoesstring;

public class FuncoesString {
    public static void main(String[] args) {
        String original = "palavras PALAVRAS palavras    ";
        String s1 = original.toLowerCase();
        String s2 = original.toUpperCase();
        String s3 = original.trim();
        String s4 = original.substring(2);
        String s5 = original.replace('a','x');

        System.out.println("----------------------------");

        // toLowerCase
        System.out.println("Sem lower case: " + original);
        System.out.println("Com lower case: " + s1);
        System.out.println("----------------------------");

        // toUpperCase
        System.out.println("Sem upper case: " + original);
        System.out.println("Com upper case: " + s2);
        System.out.println("----------------------------");

        // trim  - remove os espaços nos cantos da String
        System.out.println("Sem trim: " + original);
        System.out.println("Com trim: " + s3);
        System.out.println("----------------------------");

        // substring  -  gera uma nova String de acordo com o caractere selecionado
        System.out.println("Sem substring: " + original);
        System.out.println("Com substring: " + s4);
        System.out.println("----------------------------");

        // replace - modifica o caractere antigo por um novo, no caso todos os 'a' pelo 'x'
        System.out.println("Sem replace: " + original);
        System.out.println("Com replace: " + s5);
        System.out.println("----------------------------");

    }
}
