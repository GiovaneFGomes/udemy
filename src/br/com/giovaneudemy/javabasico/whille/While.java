package br.com.giovaneudemy.javabasico.whille;

import java.util.Scanner;

public class While {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Informe a senha até a validação");

        int senha = scan.nextInt();

        while(senha != 2002){
            System.out.println("Senha invalida");
            senha = scan.nextInt();
        }
        System.out.println("Acesso permitido");
    }
}
