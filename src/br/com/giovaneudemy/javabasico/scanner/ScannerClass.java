package br.com.giovaneudemy.javabasico.scanner;

import java.util.Scanner;

public class ScannerClass {
    public static void main(String[] args) {
        // instanciando a class Scanner
        Scanner scan = new Scanner(System.in);

        // utilizacao do Scanner com o String = scan.next();
        String x;
        x = scan.next();

        System.out.println("Você digitou: " + x);

        //------------------------------------------------------------------------------------------------------------//

        // leitura de varios dados na mesma linha separados por espaço na hora da execuçao
        String a;
        int b;
        double c;

        a = scan.next();
        b = scan.nextInt();
        c = scan.nextDouble();
        System.out.println("Dados digitados: ");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

        //------------------------------------------------------------------------------------------------------------//

        // ler um texto até a quebra de linha = nextLine();
        String s1, s2, s3;

        s1 = scan.nextLine();
        s2 = scan.nextLine();
        s3 = scan.nextLine();

        System.out.println("Dados digitados.");
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);

        //------------------------------------------------------------------------------------------------------------//

        // limpar buffer de leitura
        String a1, a2, a3;
        int a4;

        a4 = scan.nextInt();
        //scan.nextLine();   // se compilado sem nextLine, o quebra de linha do nextInt() seria da a1
        a1 = scan.nextLine();
        a2 = scan.nextLine();
        a3 = scan.nextLine();

        System.out.println("Dados digitados.");
        System.out.println(a4);
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);

        // método close() = fecha o escaneamento de leitura.
        scan.close();
        System.out.println("Scanner closed.");

    }
}
