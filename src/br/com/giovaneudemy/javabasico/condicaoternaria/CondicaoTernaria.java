package br.com.giovaneudemy.javabasico.condicaoternaria;

public class CondicaoTernaria {
    public static void main(String[] args) {
        // estrutura opcional ao if-else quando se deseja decidir um VALOR com base em uma condicao

        // em vez de fazer o if-else

        double preco = 34.5;
        double desconto;

        if(preco < 20.0){
            desconto = preco * 0.1;
        }else{
            desconto = preco * 0.05;
        }

        System.out.println(desconto);

        // utilizando condicao ternaria
        double price = 34.5;
        double discount = (price < 20.0) ? price * 0.1 : price * 0.05;
        System.out.println(discount);

    }
}
