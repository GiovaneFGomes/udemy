package br.com.giovaneudemy.javabasico.exercicios.ex1;

import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        String product01 = "Computer";
        String product02 = "Office desk";

        int age = 30;
        int code = 5290;
        char gender = 'F';

        double price01 = 2100.0;
        double price02 = 650.50;
        double measure = 53.234567;

        System.out.println("Products: ");
        System.out.println(product01 + ",which price is $ " + price01);
        System.out.println(product02 + ",which price is $ " + price02);

        System.out.printf("\nRecord: %d years old, code %d and gender: %s\n\n", age,code,gender);

        System.out.printf("Measure with eight decimal places: %.8f\n", measure);
        System.out.printf("Rouded (three decimal places: %.3f\n", measure);
        
        Locale.setDefault(Locale.US);
        System.out.printf("US decimal point: %.3f%n", measure);

        // \n and %n = broke line.
    }
}
